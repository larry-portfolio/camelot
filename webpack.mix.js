const mix = require("laravel-mix")

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const path = require("path")
const resolve = dir => path.resolve(__dirname, dir)

mix
  .js("resources/js/app.js", "public/js")
  .sass("resources/sass/app.scss", "public/css")
  .options({
    extractVueStyles: true,
    globalVueStyles: "resources/sass/variables.scss"
  })
  .webpackConfig({
    resolve: {
      extensions: [".js", ".vue", ".json"],
      alias: {
        Components: resolve("resources/js/components"),
        Icons: resolve("resources/js/icons"),
        Images: resolve("public/img"),
        Plugins: resolve("resources/js/plugins"),
        Sass: resolve("resources/sass"),
        Store: resolve("resources/js/store")
      }
    }
  })
