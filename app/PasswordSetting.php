<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordSetting extends Model
{
    /**
     * Get the status of the audit trail.
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'amount', 'status_id'];
}
