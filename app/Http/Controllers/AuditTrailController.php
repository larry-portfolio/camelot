<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use Illuminate\Http\Request;

class AuditTrailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auditTrailCols = json_encode([
            ['id' => 1, 'name' => 'Audit Code', 'in_db' => 'audit_code', 'sort' => true],
            ['id' => 2, 'name' => 'Username', 'in_db' => 'username', 'sort' => true],
            ['id' => 3, 'name' => 'Role', 'in_db' => 'role', 'sort' => true],
            ['id' => 4, 'name' => 'Form', 'in_db' => 'form', 'sort' => true],
            ['id' => 5, 'name' => 'Timestamp', 'in_db' => 'created_at', 'sort' => true],
            ['id' => 6, 'name' => 'Status', 'in_db' => 'status_id', 'sort' => true],
            ['id' => 7, 'name' => 'Description', 'in_db' => 'description', 'sort' => false],
        ]);
        $auditTrails = AuditTrail::with('status')->orderBy('audit_code')->get();

        return view('section.audit-trails', [
            'auditTrailCols' => $auditTrailCols,
            'auditTrails' => $auditTrails,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuditTrail  $auditTrail
     * @return \Illuminate\Http\Response
     */
    public function show(AuditTrail $auditTrail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuditTrail  $auditTrail
     * @return \Illuminate\Http\Response
     */
    public function edit(AuditTrail $auditTrail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuditTrail  $auditTrail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuditTrail $auditTrail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuditTrail  $auditTrail
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditTrail $auditTrail)
    {
        //
    }
}
