<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\PasswordSetting;
use App\Status;
use App\User;
use Auth;
use Illuminate\Http\Request;

class SecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passwordSettings = PasswordSetting::with('status')->get();

        foreach ($passwordSettings as $item) {
            if ($item->status->name === 'Active') {
                $item->status = true;
            } else if ($item->status->name === 'Inactive') {
                $item->status = false;
            }
        }

        return view('section.security', [
            'passwordSettings' => $passwordSettings,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $reqArray = [];
        $activeArray = [];
        $inactiveArray = [];
        $activeList = "";
        $inactiveList = "";
        $activeStart = 'Active password setting values:';
        $inactiveStart = 'Inactive password setting values:';

        $activeStatus = Status::where('name', 'Active')->pluck('id')->first();
        $inactiveStatus = Status::where('name', 'Inactive')->pluck('id')->first();

        // Transform request object to array
        foreach ($request->request as $key => $value) {
            $reqArray += [$key => $value];
        }

        // Create two arrays for request with and without status
        $withStatus = array_filter($reqArray, function ($var) {
            return $var === 'on';
        });
        $withoutStatus = array_filter($reqArray, function ($var) {
            return $var !== 'on';
        });

        // Remove _status in key
        foreach ($withStatus as $key => $value) {
            $newKey = rtrim($key, '_status');
            $withStatus[$newKey] = $withStatus[$key];
            unset($withStatus[$key]);
        }

        // Create an array with active status
        foreach ($withoutStatus as $key => $value) {
            if (array_key_exists($key, $withStatus)) {
                $activeArray += [$key => $value];
            }
        }

        // Create an array with inactive status
        foreach ($withoutStatus as $key => $value) {
            unset($inactiveArray['_token']);
            if (!array_key_exists($key, $activeArray)) {
                $inactiveArray += [$key => $value];
            }
        }

        // Update active password settings in table
        foreach ($activeArray as $key => $value) {
            PasswordSetting::where('name', $key)
                ->update(['amount' => $value, 'status_id' => $activeStatus]);
            $activeList .= ' ' . ucfirst($key) . ' = ' . $value . ',';
        }

        // Update inactive password settings in table
        foreach ($inactiveArray as $key => $value) {
            PasswordSetting::where('name', $key)
                ->update(['amount' => $value, 'status_id' => $inactiveStatus]);
            $inactiveList .= ' ' . ucfirst($key) . ' = ' . $value . ',';
        }

        $activeList = rtrim($activeList, ',');
        $inactiveList = rtrim($inactiveList, ',');

        $auditTrail = [
            'audit_code' => "LOG" . sprintf('%07d', AuditTrail::orderBy('id')->get()->last()->id + 1 ?? 1),
            'username' => User::where('id', Auth::id())->pluck('username')->first(),
            'role' => 'Admin',
            'form' => 'Edit Password Settings',
            'created_at' => now(),
            'status_id' => Status::where('name', 'Success')->pluck('id')->first(),
            'description' => $activeStart . $activeList . ".\n" . $inactiveStart . $inactiveList . '.',
        ];

        AuditTrail::insert($auditTrail);

        return redirect('/security')
            ->with('message', 'Password settings have been updated.');
    }
}
