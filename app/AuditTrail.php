<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    /**
     * Get the status of the audit trail.
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'audit_code', 'username', 'role', 'form', 'created_at', 'status_id', 'description',
    ];

    /**
     * Disable timestamp fields.
     */
    public $timestamps = false;
}
