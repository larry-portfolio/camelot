<?php

use App\PasswordSetting;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasswordSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('password_settings', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->unsignedTinyInteger('amount');
            $table->string('description');
            $table->unsignedTinyInteger('status_id');
            $table->timestampsTz();
            $table->softDeletesTz();

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });

        $passwordSettings = [
            $this->addPasswordSetting('minimum', 8, 'Minimum password length'),
            $this->addPasswordSetting('uppercase', 1, 'Uppercase letter required'),
            $this->addPasswordSetting('lowercase', 1, 'Lowercase letter required'),
            $this->addPasswordSetting('number', 1, 'Number or digit required'),
            $this->addPasswordSetting('special', 0, 'Special character required'),
            $this->addPasswordSetting('expiry', 60, 'Password expiration in days'),
        ];

        PasswordSetting::insert($passwordSettings);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_settings');
    }

    /**
     * Add a password setting with current timestamps.
     *
     * @param string
     * @return string[]
     */
    private function addPasswordSetting($name, $amount, $description)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        return [
            'name' => $name,
            'amount' => $amount,
            'description' => $description,
            'status_id' => 3,
            'created_at' => $now,
            'updated_at' => $now,
        ];
    }
}
