<?php

use App\Status;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        $statuses = [
            $this->addStatus('Success'),
            $this->addStatus('Error'),
            $this->addStatus('Active'),
            $this->addStatus('Inactive'),
        ];

        Status::insert($statuses);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }

    /**
     * Add a status with current timestamps.
     *
     * @param string
     * @return string[]
     */
    private function addStatus($name)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        return ['name' => $name, 'created_at' => $now, 'updated_at' => $now];
    }
}
