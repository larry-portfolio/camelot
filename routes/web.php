<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/audit-trails', 'AuditTrailController@index')->name('audit-trails');
Route::get('/security', 'SecurityController@index')->name('security');

Route::post('/security', 'SecurityController@update')->name('security');
