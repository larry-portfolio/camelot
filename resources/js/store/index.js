import Vue from "vue"
import Vuex from "vuex"
import Axios from "axios"
import { Parse } from "Plugins/DataParser"

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    form: [],
    users: [],
    navHidden: true
  },
  mutations: {
    FETCH_USERS: (state, users) => {
      state.users = users
    },
    CREATE_USER: (state, obj) => {
      state.users.unshift(Parse(obj))
    },
    TOGGLE_NAV: state => {
      state.navHidden = !state.navHidden
    }
  },
  actions: {
    register({ commit }, data) {
      Axios.post("/register", data)
        .then(result => commit("CREATE_USER", result))
        .catch(error => console.log("Register Action:", error))
    },
    fetchUsers({ commit }) {
      Axios.get("/users")
        .then(result => commit("FETCH_USERS", result.data))
        .catch(error => console.log("Fetch User Action:", error))
    },
    toggleNav({ commit }) {
      commit("TOGGLE_NAV")
    }
  },
  modules: {}
})

export default store
