import AppContainer from "Components/AppContainer.vue"
import AppFooter from "Components/AppFooter.vue"
import AppNavbar from "Components/AppNavbar.vue"
import AppSidebar from "Components/AppSidebar.vue"
import BaseButton from "Components/BaseButton.vue"
import BaseForm from "Components/BaseForm.vue"
import BaseTile from "Components/BaseTile.vue"
import BaseHeader from "Components/BaseHeader.vue"
import DataTable from "Components/DataTable.vue"
import FormCheckbox from "Components/FormCheckbox.vue"
import FormInput from "Components/FormInput.vue"
import NavLink from "Components/NavLink.vue"
import NavToggle from "Components/NavToggle.vue"

const components = {
  AppContainer,
  AppFooter,
  AppNavbar,
  AppSidebar,
  BaseButton,
  BaseForm,
  BaseTile,
  BaseHeader,
  DataTable,
  FormCheckbox,
  FormInput,
  NavLink,
  NavToggle
}

export default components
