@extends('layouts.app') @section('content')
<app-container>
  <base-header title="Audit Trails">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis,
    aliquid facere? In perferendis incidunt mollitia necessitatibus voluptas!
  </base-header>
  <data-table
    columns="{{ $auditTrailCols }}"
    data="{{ $auditTrails }}"
  ></data-table>
</app-container>
@endsection
