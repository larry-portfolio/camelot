@extends('layouts.app') @section('content')
<app-container>
  @if (session('message'))
  <div>{{ session("message") }}</div>
  @endif

  <base-header title="Security">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis,
    aliquid facere? In perferendis incidunt mollitia necessitatibus voluptas!
  </base-header>
  <base-form action="/security">
    @csrf @foreach($passwordSettings as $item)
    <app-container style="margin: 0 1rem;">
      <form-checkbox
        name="{{ $item->name . '_status' }}"
        label="{{ $item->description }}"
        value="{{ $item->status }}"
      ></form-checkbox>
      <form-input
        type="number"
        name="{{ $item->name }}"
        value="{{ $item->amount }}"
      ></form-input>
    </app-container>
    @endforeach

    <base-button type="submit">{{ __("Save") }}</base-button>
  </base-form>
</app-container>
@endsection
