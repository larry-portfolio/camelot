@extends('layouts.app') @section('content')
<app-container>
  @if (session('message'))
  <div class="alert alert-success" role="alert">
    {{ session("message") }}
  </div>
  @endif
  <!-- You are logged {{ session("user.name") }}! -->
  <base-header title="Camelot Integrated Management System">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis,
    aliquid facere? In perferendis incidunt mollitia necessitatibus voluptas!
    Nobis fugit ipsam consequuntur quis error, molestiae, tenetur magnam atque
    suscipit non odit!
  </base-header>

  <base-tile disabled icon="users">Clients</base-tile>
  <base-tile disabled icon="handshake">Contracts</base-tile>
  <base-tile disabled icon="cogs">Services</base-tile>
  <base-tile disabled icon="file-invoice-dollar">Pricing</base-tile>
  <base-tile disabled icon="landmark">Agencies</base-tile>
  <base-tile disabled icon="id-card">Users</base-tile>
  <base-tile disabled icon="theater-masks">Roles</base-tile>
  <base-tile icon="shield-alt">Security</base-tile>
  <base-tile icon="history">Audit Trails</base-tile>
  <base-tile disabled icon="user-circle">My Account</base-tile>
</app-container>
@endsection
