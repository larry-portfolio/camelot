@extends('layouts.app') @section('content')
<app-container>
  <base-form action="{{ route('register') }}">
    <!-- <form method="POST" action="{{ route('register') }}"> -->
    @csrf

    <form-input
      name="name"
      icon="user-alt"
      placeholder="Name"
      required
      autofocus
    ></form-input>
    <!-- value="{{ old('name') }}" -->
    <!-- autocomplete="name" -->

    @error('name')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <form-input
      name="username"
      icon="user-alt"
      placeholder="Username"
      required
    ></form-input>
    <!-- value="{{ old('name') }}" -->
    <!-- autocomplete="name" -->

    @error('name')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <form-input
      name="email"
      type="email"
      icon="envelope"
      placeholder="Email Address"
      required
    ></form-input>
    <!-- autocomplete="email" -->
    <!-- value="{{ old('email') }}" -->

    @error('email')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <form-input
      name="password"
      type="password"
      icon="key"
      placeholder="Password"
      required
    ></form-input>
    <!-- autocomplete="new-password" -->

    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <form-input
      name="password_confirmation"
      type="password"
      icon="key"
      placeholder="Confirm Password"
      required
    ></form-input>
    <!-- autocomplete="new-password" -->

    <base-button type="submit">{{ __("Register") }}</base-button>
    <!-- {{ config("app.name") }} -->
    <!-- </form> -->
  </base-form>
</app-container>
@endsection
