@extends('layouts.app') @section('content')
<app-container>
  <base-form action="{{ route('login') }}">
    @csrf

    <form-input
      name="email"
      type="email"
      icon="envelope"
      placeholder="Email Address"
      required
      autofocus
    ></form-input>
    <!-- autocomplete="email" -->
    <!-- value="{{ old('email') }}" -->

    @error('email')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <form-input
      name="password"
      type="password"
      icon="key"
      placeholder="Password"
      required
    ></form-input>
    <!-- autocomplete="current-password" -->

    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror

    <div class="form-group row">
      <div class="col-md-6 offset-md-4">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="remember"
          id="remember"
          {{ old("remember") ? "checked" : "" }}>

          <label class="form-check-label" for="remember">
            {{ __("Remember Me") }}
          </label>
        </div>
      </div>
    </div>

    <base-button type="submit">{{ __("Login") }}</base-button>

    @if (Route::has('password.request'))
    <a class="btn btn-link" href="{{ route('password.request') }}">
      {{ __("Forgot Your Password?") }}
    </a>
    @endif
  </base-form>
</app-container>
@endsection
