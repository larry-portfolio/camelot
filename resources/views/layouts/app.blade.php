<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config("app.name") }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
  </head>
  <body>
    <div id="vue">
      <app-navbar brand-name="{{ config('app.name') }}"></app-navbar>
      <app-sidebar></app-sidebar>
      <!-- @guest
      <a href="{{ route('login') }}">{{ __("Login") }}</a>
      @if (Route::has('register'))
      <a href="{{ route('register') }}">{{ __("Register") }}</a>
      @endif @else
      <a
        href="{{ route('logout') }}"
        onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"
      >
        {{ __("Logout") }}
      </a>
      <form
        id="logout-form"
        action="{{ route('logout') }}"
        method="POST"
        style="display: none;"
      >
        @csrf
      </form>
      @endguest -->

      <main>@yield('content')</main>

      <app-footer year="2020">
        Camelot Integrated Management System
      </app-footer>
    </div>
  </body>
</html>
